package epam;

public class Main {
    public static void main(String[] args) {
        Rumors rumors = new Rumors();
        final int maxIteration = 100;
        int peopleNumber = rumors.inputData();
        int spreadTime = 0;
        int peopleNumbers = 0;
        for (int i = 0; i < maxIteration; i++) {
            boolean guests[] = new boolean[peopleNumber];
            guests[1] = true; //Bob is first heard rumor
            boolean isAlreadyHeard = false;
            int newPerson = -1;
            int currentPerson = 1;
            while (!isAlreadyHeard) {
                newPerson = rumors.getRandomPerson(peopleNumber);
                if (newPerson == currentPerson) {
                    while (newPerson == currentPerson)
                        newPerson = rumors.getRandomPerson(peopleNumber);
                }
                // Check if new guest is already heard rumor
                if (guests[newPerson]) {
                    if (rumors.checkFullSpreaded(guests))
                        spreadTime++;
                    peopleNumbers += rumors.calcCountPeople(guests);
                    isAlreadyHeard = true;
                }
                guests[newPerson] = true;
                currentPerson = newPerson;
            }
        }
        rumors.outputStatistics(spreadTime, peopleNumbers, maxIteration);
    }
}
