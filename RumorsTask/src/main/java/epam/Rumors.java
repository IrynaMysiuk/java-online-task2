package epam;

import java.util.Scanner;

public class Rumors {

    public int inputData() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of people at the party (N > 2): ");
        int questNumbers = scanner.nextInt();
        if (questNumbers <= 2) {
            throw new RuntimeException("Quests is the less than 2 at the party!");
        }
        return questNumbers;
    }

    public int getRandomPerson(int peopleNumber) {
        return 1 + (int) (Math.random() * (peopleNumber - 1)); //randomize next person
    }

    public int calcCountPeople(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }

    public boolean checkFullSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }

    public void outputStatistics(int spreadedTime, int peopleNumbers, int maxIteration) {
        System.out.println(String.format("Probability that everyone will hear rumor (except Alice) in %s attempts: %s",
                maxIteration, (double) spreadedTime / maxIteration));
        System.out.println("Average number of people that rumor reached is: " + peopleNumbers / maxIteration);
    }

}

